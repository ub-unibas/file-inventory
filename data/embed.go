package data

import (
	"embed"
)

//go:embed default.sig
var SiegfriedSig []byte

//go:embed config.toml
var ConfigFS embed.FS
