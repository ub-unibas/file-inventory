package cmd

import (
	"fmt"
	ironmaiden "github.com/je4/indexer/v2/pkg/indexer"
	lm "github.com/je4/utils/v2/pkg/logger"
	"github.com/spf13/cobra"
	"gitlab.switch.ch/ub-unibas/file-inventory/v2/data"
	"gitlab.switch.ch/ub-unibas/file-inventory/v2/pkg/inventory"
	"golang.org/x/exp/slices"
	"io/fs"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const VERSION = "v1.0-beta.1"

const LOGFORMAT = `%{time:2006-01-02T15:04:05.000} %{shortpkg}::%{longfunc} [%{shortfile}] > %{level:.5s} - %{message}`

var rootCmd = &cobra.Command{
	Use:   "inventory",
	Short: "inventory creates an inventory list of a file system",
	Long: fmt.Sprintf(`A fast and reliable filesystem indexer
Jürgen Enge (University Library Basel, juergen.enge@unibas.ch)
Version %s`, VERSION),
	Args: cobra.MaximumNArgs(1),
	Run:  doRoot,
}

func init() {
	initRoot()
}

func initRoot() {
	rootCmd.Flags().StringP("config", "c", "", "path to config file")
	rootCmd.Flags().StringP("output", "o", "", "output file")
	rootCmd.Flags().StringP("format", "f", "json", "output format (JSON or CSV)")
	rootCmd.Flags().UintP("number-of-workers", "n", 1, "number of concurrent inventory workers")
}

func doRoot(cmd *cobra.Command, args []string) {
	daLogger, lf := lm.CreateLogger("file-inventory", "", nil, "DEBUG", LOGFORMAT)
	defer lf.Close()

	configFile, err := cmd.Flags().GetString("config")
	if err != nil {
		daLogger.Errorf("cannot get cmd flag 'config': %v", err)
		_ = cmd.Help()
		return
	}
	var configFS fs.FS
	var configPath string
	if configFile == "" {
		configFS = data.ConfigFS
		configPath = "config.toml"
	} else {
		if !filepath.IsAbs(configFile) {
			current, err := os.Getwd()
			if err != nil {
				daLogger.Errorf("cannot get current dir: %v", err)
				return
			}
			configFile = filepath.Join(current, configFile)
		}
		configFS = os.DirFS(filepath.Base(configFile))
		configPath = filepath.Dir(configFile)
	}
	config, err := LoadConfig(configFS, configPath)
	if err != nil {
		daLogger.Errorf("cannot load config: %v", err)
		return
	}

	output, err := cmd.Flags().GetString("output")
	if err != nil {
		daLogger.Errorf("cannot get cmd flag 'output': %v", err)
		_ = cmd.Help()
		return
	}
	format, err := cmd.Flags().GetString("format")
	if err != nil {
		daLogger.Errorf("cannot get cmd flag 'format': %v", err)
		_ = cmd.Help()
		return
	}
	format = strings.ToLower(format)
	if !slices.Contains([]string{"json", "csv"}, format) {
		daLogger.Errorf("invalid format '%s'", format)
		_ = cmd.Help()
		return
	}

	num, err := rootCmd.Flags().GetUint("number-of-workers")
	if err != nil {
		daLogger.Errorf("cannot get cmd flag 'number-of-workers': %v", err)
		_ = cmd.Help()
		return
	}

	var relevance = map[int]ironmaiden.MimeWeightString{}
	if config.Indexer.MimeRelevance != nil {
		for key, val := range config.Indexer.MimeRelevance {
			num, err := strconv.Atoi(key)
			if err != nil {
				daLogger.Errorf("cannot convert mimerelevance key '%s' to int", key)
				continue
			}
			relevance[num] = val
		}
	}
	ad := ironmaiden.NewActionDispatcher(relevance)
	var signature []byte
	if config.Indexer.Siegfried.Signature == "" {
		signature = data.SiegfriedSig
	} else {
		signature, err = os.ReadFile(config.Indexer.Siegfried.Signature)
		if err != nil {
			daLogger.Errorf("cannot read siegfried signature file '%s': %v", config.Indexer.Siegfried.Signature, err)
			return
		}
	}
	_ = ironmaiden.NewActionSiegfried("siegfried", signature, config.Indexer.Siegfried.MimeMap, nil, ad)
	daLogger.Info("indexer action siegfried added")

	var source string
	if len(args) > 0 {
		source = args[0]
	}

	source, err = filepath.Abs(source)
	if err != nil {
		daLogger.Errorf("cannot get absolute path from '%s': %v", source, err)
		return
	}
	fsys := os.DirFS(source)
	if err := inventory.Walk(fsys, "", output, format, num, ad, daLogger); err != nil {
		daLogger.Errorf("error walking '%v': %v", fsys, err)
		return
	}
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
