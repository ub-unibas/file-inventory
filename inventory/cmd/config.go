package cmd

import (
	"emperror.dev/errors"
	"github.com/BurntSushi/toml"
	ironmaiden "github.com/je4/indexer/v2/pkg/indexer"
	"io/fs"
)

type Siegfried struct {
	Signature string            `toml:"signature"`
	MimeMap   map[string]string `toml:"mimemap"`
}

type Indexer struct {
	Enabled       bool                                   `toml:"enable"`
	MimeRelevance map[string]ironmaiden.MimeWeightString `toml:"mimerelevance"`
	Siegfried     Siegfried                              `toml:"siegfried"`
}

type Config struct {
	Indexer Indexer `toml:"indexer"`
}

func LoadConfig(fsys fs.FS, path string) (*Config, error) {
	cfgFile, err := fsys.Open(path)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot open %s/%s", fsys, path)
	}
	defer cfgFile.Close()

	var config = Config{}

	if _, err := toml.DecodeFS(fsys, path, &config); err != nil {
		return nil, errors.Wrapf(err, "cannot decode %s/%s", fsys, path)
	}
	return &config, nil
}
