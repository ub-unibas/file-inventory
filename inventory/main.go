package main

import "gitlab.switch.ch/ub-unibas/file-inventory/v2/inventory/cmd"

func main() {
	cmd.Execute()
}
