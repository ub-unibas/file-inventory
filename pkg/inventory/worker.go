package inventory

import (
	"emperror.dev/errors"
	"github.com/je4/utils/v2/pkg/checksum"
	"io/fs"
)

type Result struct {
	Path     string `json:"path"`
	Digests  map[checksum.DigestAlgorithm]string
	Pronom   string
	Mimetype string
	Indexer  map[string]any
	Err      error
	Size     int64
}

func worker(fsys fs.FS, done <-chan struct{}, paths <-chan string, result chan<- *Result) {
	for path := range paths {
		r, err := func(p string) (*Result, error) {
			var r = &Result{}
			fp, err := fsys.Open(path)
			if err != nil {
				return nil, errors.Wrapf(err, "cannot open '%s/%s'", fsys, path)
			}
			stat, err := fp.Stat()
			if err != nil {
				return nil, errors.Wrapf(err, "cannot get stat from '%s/%s'", fsys, path)
			}
			r.Size = stat.Size()
			csWriter, err := checksum.NewChecksumWriter([]checksum.DigestAlgorithm{checksum.DigestSHA512})
			if err != nil {
				return nil, errors.Wrap(err, "cannot create checksumwriter")
			}
			_ = csWriter

			return r, nil
		}(path)
		if err != nil {
			r = &Result{Err: err}
		}
		select {
		case result <- r:
		case <-done:
			return
		}
	}
}
